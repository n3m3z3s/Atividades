package cest.mypet.cadastro.loc;

import cest.mypet.cadastro.loc.UF;


public class Cidade{
	
	private UF uf; 
	private String nome;
	
	public UF getUf() {
		return uf;
	}
	public void setUf(UF uf) {
		this.uf = uf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
