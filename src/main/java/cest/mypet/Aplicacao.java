package cest.mypet;

import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.Pessoa;
import cest.mypet.cadastro.TipoAnimal;
import cest.mypet.cadastro.loc.Cidade;
import cest.mypet.cadastro.loc.UF;

public class Aplicacao {
	

	public static void main(String[] args) {
		
		TipoAnimal raca = new TipoAnimal();
		raca.setCod(1);
		raca.setDescricao("Vira-lata");
		
		Animal a1 = new Animal();
		a1.setNome("Rex");
		a1.setIdade(5);
		a1.setTipo(raca);
		
		UF u = new UF();
		u.setCod("1");
		u.setDescricao("MA");
		
		
		Cidade c1 = new Cidade();
		c1.setNome("Sao luis");
		c1.setUf(u);
	
		
		Pessoa p1 = new Pessoa();
		p1.setNome("Paulo");
		p1.setIdade(30);
		p1.setCidade(c1);
	
	
		System.out.println("a1 - Nome: " + a1.getNome());
		System.out.println("a1 - Idade: " + a1.getIdade());
		System.out.println("a1 - Tipo: " + a1.getTipo().getDescricao());
		System.out.println("a1 - Código: " + a1.getTipo().getCod());
		System.out.println("");
		
		System.out.println("p1 - Nome: " + p1.getNome());
		System.out.println("p1 - Idade: " + p1.getIdade());
		System.out.println("p1 - Cidade: " + p1.getCidade().getNome());
		System.out.println("");
		
		
		System.out.println("c1 - Nome: " + c1.getNome());
		System.out.println("c1 - UF: " + c1.getUf().getDescricao());
		System.out.println("");
		
		
		}



}
