import java.util.ArrayList;
import java.util.Arrays;

public class TestArr {
	public static void main (String[] args) 
    {
        int a1[] = {1,2,4,8,16,32,64,128};
        int a2[] = {2,4,8,16,32,64};
        if (Arrays.equals(a1, a2))
            System.out.println("Os arrays são guais!!!");
        else
            System.out.println("Os arrays são diferentes!!!");
        
        ArrayList lista1 = new ArrayList();
        lista1.add("Texto");
        lista1.add("Java");
        lista1.add(100);
        lista1.add(14);
        
        ArrayList lista2 = new ArrayList();
        lista2.add("Teste1");
        lista2.add(3);
        lista2.add(5000);
        lista2.add("OOP");
        
        
        System.out.println("LISTA 1 " + lista1);
        System.out.println("LISTA 2 " + lista2);
        
        System.out.println("Este item foi removido da LISTA 1: " + lista1.remove(0));
        System.out.println("LISTA 1: " + lista1);
        System.out.println("Este item foi removido da LISTA 2: " + lista2.remove(3));
        System.out.println("LISTA 1: " + lista2);
        
        lista2.add("Programação orientada a objetos");
        lista1.add(100000);
        
        System.out.println("Adcionado um item em LISTA 2: " + lista2);
        System.out.println("Adcionado um item em LISTA 1: " + lista1);
        
    }
}